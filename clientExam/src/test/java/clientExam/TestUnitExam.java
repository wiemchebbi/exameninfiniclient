package clientExam;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import entities.Bugs;
import entities.Commentaire;
import entities.Users;
import services.ExamServiceEJBRemote;

public class TestUnitExam {
	ExamServiceEJBRemote remote;

	@Before
	public void init() {
		try {
			Context context = new InitialContext();

			remote = (ExamServiceEJBRemote) context
					.lookup("/examenINFINIServer/ExamServiceEJB!services.ExamServiceEJBRemote");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void case1() {
		Users users = new Users();
		users.setNom("ahmed");
		users.setPrenom("ben ahmed");
		Bugs bug1 = new Bugs(203, "normal", 202, "bug en production");
		Bugs bug2 = new Bugs(204, "High", 203, "bug en ligne");
		List<Bugs> bugs = new ArrayList<Bugs>();
		bugs.add(bug1);
		bugs.add(bug2);
		users.affectUserToBugs(bugs);
		users.setBugs(bugs);
		remote.addUsers(users);
	}

	@Test
	public void case2() {
		Commentaire commentaire1 = new Commentaire();
		commentaire1
				.setText_commentaire("Vous devez faire un refresh après un commit");
		Commentaire commentaire2 = new Commentaire();
		commentaire2.setText_commentaire("Les transactions sont géré par JTA ");
		List<Commentaire> commentaires = new ArrayList<Commentaire>();
		commentaires.add(commentaire1);
		commentaires.add(commentaire2);
		remote.addCommentToBugs(204, commentaires);
	}

	@Test
	public void case3() {
		List<Bugs> listBugs = remote.getHistoryBugs();
		for (Bugs bugs : listBugs) {
			List<Commentaire> commentaires = bugs.getCommentaires();
			for (Commentaire commentaire : commentaires) {
				System.out.println("Bugs|\t" + bugs.getNumero_bug()
						+ "\tutilisateur|\t" + bugs.getUsers().getNom()
						+ "\tcommentaires|\t"
						+ commentaire.getText_commentaire());
			}

		}
	}

}
